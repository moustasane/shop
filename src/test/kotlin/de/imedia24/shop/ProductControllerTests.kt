package de.imedia24.shop

import com.fasterxml.jackson.databind.ObjectMapper
import de.imedia24.shop.controller.ProductController
import de.imedia24.shop.domain.product.ProductReqeust
import de.imedia24.shop.domain.product.ProductResponse
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.restdocs.RestDocsRestAssuredConfigurationCustomizer
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.patch
import org.springframework.test.web.servlet.post
import java.math.BigDecimal

@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTests  {

    @Autowired
    lateinit var  mockMvc: MockMvc
    @Autowired
    lateinit var  mapper: ObjectMapper

        @Test
        fun `Should return product with Sku=1` () {


            val resp = ProductResponse("1", "product1", description = "description prod1", BigDecimal.valueOf(100))
            mockMvc.get("/products/1").andDo {
                print()
            }
            .andExpect {
                status { isOk()  }
                content { contentType(MediaType.APPLICATION_JSON_UTF8) }
                content { json(mapper.writeValueAsString(resp)) }


            }
    }


    @Test
    fun `Should return product with Sku=1,2` () {



        mockMvc.get("/products?skus=1,2").andDo {
            print()
        }
                .andExpect {
                    status { isOk()  }
                    content { contentType(MediaType.APPLICATION_JSON_UTF8) }
                    jsonPath("$[0].sku") { value(1)}
                    jsonPath("$[1].sku") { value(2)}



                }
    }


    @Test
    fun `Should return product with Sku=4` () {


        val reqeust = ProductReqeust("4","product4","444",BigDecimal(400))
        mockMvc.post("/products"){
            contentType = MediaType.APPLICATION_JSON
            content  = mapper.writeValueAsString(reqeust)
        }.andDo {
            print()
        }
                .andExpect {
                    status { isOk()  }
                    content { contentType(MediaType.APPLICATION_JSON_UTF8) }
                    jsonPath("$.sku") { value("4")}
                    jsonPath("$.price") { value(400)}


                }
    }


    @Test
    fun `Should return product without changing sku` () {


        val reqeust = ProductReqeust("10","product3","444",BigDecimal(400))
        mockMvc.patch("/products/3"){
            contentType = MediaType.APPLICATION_JSON
            content  = mapper.writeValueAsString(reqeust)
        }.andDo {
            print()
        }
                .andExpect {
                    status { isOk()  }
                    content { contentType(MediaType.APPLICATION_JSON_UTF8) }
                    jsonPath("$.sku") { value("3")}
                    jsonPath("$.price") { value(400)}
                    jsonPath("$.description") { value("444")}


                }
    }


}
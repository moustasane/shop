INSERT INTO products values
    (
        '1',
        'product1',
        'description prod1',
        100,
        CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP
    );

INSERT INTO products values
    (
        '2',
        'product2',
        'description prod2',
        200,
        CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP
    );

INSERT INTO products values
    (
        '3',
        'product3',
        'description prod3',
        300,
        CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP
    );

package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductReqeust
import de.imedia24.shop.domain.product.ProductResponse
import org.springframework.stereotype.Service

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {

        val product = productRepository.findBySku(sku)
        if(product!=null){


            return ProductResponse(product.sku,product.name,product.description,product.price)
        }
        return null;
    }


    fun findProductBySkuIn(sku: List<String>): List<ProductResponse>? {

        val products = productRepository.findBySkuIn(sku)
        var responseProducts = mutableListOf<ProductResponse>()
        if(products!=null){

            for (product in products){
                responseProducts.add(ProductResponse(product.sku,product.name,product.description,product.price))


            }

            return responseProducts;
        }
        return null;
    }

    fun addProduct(product: ProductReqeust): ProductResponse? {
        var productToAdd = ProductEntity()

        productToAdd = productToAdd.copy(sku =
        product.sku, name = product.name, description = product.description, price = product.price)


        try {
            productToAdd = productRepository.save(productToAdd);
            return ProductResponse(productToAdd.sku,productToAdd.name,productToAdd.description,productToAdd.price)
        }catch (e : Exception){


            return null

        }






    }

    fun patchProduct(sku: String, product: ProductReqeust): ProductResponse? {
        val originalProduct = productRepository.findBySku(sku)


        var productToPatch = originalProduct?.copy(name = product.name, description = product.description, price = product.price)

        try {
            if(productToPatch!=null){
            productToPatch = productRepository.save(productToPatch);



                return ProductResponse(productToPatch.sku,productToPatch.name,productToPatch.description,productToPatch.price)
            }
        }catch (e : Exception){


            return null

        }

        return null
    }
}

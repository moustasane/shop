package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductReqeust
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.websocket.server.PathParam

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }


    @GetMapping("/products", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
            @RequestParam("skus") skus: List<String>
    ): ResponseEntity<List<ProductResponse>> {
        logger.info("Request for products ${skus}")

        val products = productService.findProductBySkuIn(skus)
        return if(products == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }


    @PostMapping("/products", produces = ["application/json;charset=utf-8"])
    fun addProducts(
            @RequestBody product: ProductReqeust
    ): ResponseEntity<ProductResponse> {
        logger.info("Request to add product $product")

        val product = productService.addProduct(product)
        return if(product == null) {
            ResponseEntity.status(203).body(product)
        } else {
            ResponseEntity.ok(product)
        }
    }


    @PatchMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun UpdateProducts(@PathVariable("sku") sku: String,
            @RequestBody product: ProductReqeust
    ): ResponseEntity<ProductResponse> {
        logger.info("Request to path product $sku new values $product")

        val product = productService.patchProduct(sku,product)
        return if(product == null) {
            ResponseEntity.status(203).body(product)
        } else {
            ResponseEntity.ok(product)
        }
    }
}

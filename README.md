# iMedia24 Coding challenge
### Swagger Documentation
   Docs :
    * [Swagger UI](http://localhost:8080/swagger-ui/index.html#/)

### Reference Documentation
For further reference, please consider the following sections:
### Doker 
## Build
Build Docker conteiner :
```docker build  -t imedia24/shop-docker .  ```
Docker Hub repository : 

[Docker Hub repository ](https://hub.docker.com/r/moustahsane/imedia24-shop)

## Pull Docker Image

``` docker pull moustahsane/imedia24-shop ```

## Run Docker container

``` docker run -p 8080:8080 moustahsane/imedia24-shop ```

### Rest Api Documentation
Visit link :
[Swagger Document](http://localhost:8080/swagger-ui/index.html)





